from DataTool.Oven import Oven
from DataTool.CookDay import CookDay
from DataTool.data_helpers import butter_lowpass_filter
from DataTool.Ovens import Ovens
import pprint
import matplotlib.pyplot as plt
import numpy as np
import argparse
import os
from os import path


def do_oven_plotting(plot_comparison=False):
    start = "2021.07.06 10:00:00"
    end = "2021.07.06 18:00:00"
    ovens = Ovens(start_time = start, end_time = end)
    ovens.plot_all_ovens()
    # ovens.plot_air_comparison()


def do_save_data():
    folder_path = r'C:\Users\ArikJenkins\Serve Automation\Serve Main - Engineering\13 Testing\Logs\20210401'
    start = "2021.04.01 11:00:00"
    end = "2021.04.01 18:30:00"

    for i in range(1, 5):
        print(i)
        oven = Oven(f'oven-{i}', start, end)
        print(f'saving data for oven {i}')
        oven.save_oven_data(folder_path)


def do_cook_day(args):
    cook_day = CookDay(date=args.date)
    # base_path = r'C:\Users\ArikJenkins\Serve Automation\Serve Main - Engineering\13 Testing\Logs\20210701\images'
    # dampers = ['damper_0','damper_2','damper_5','damper_7','damper_10','damper_20']
    # for i in dampers:
        # cook_day.sort_images_by_oven(os.path.abspath(os.path.join(base_path, i)))
    cook_day.sort_images_by_oven()
    # pp.pprint(cook_day)
    # for pizza in cook_day.pizzas:
    #     # pp.pprint(pizza.__dict__)
    #     pizza.plot_temps()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--date', type=str, default='2021.08.31')
    args = parser.parse_args()

    do_cook_day(args=args)
    # do_oven_plotting(plot_comparison=False)
    # do_save_data()
