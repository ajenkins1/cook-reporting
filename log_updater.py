import requests
import json
import argparse
import os
from os import path
from datetime import datetime

import pandas as pd
# import datetime as dt
from openpyxl import load_workbook


def archive_pizza_data(folder_path, json_path):
    with open(json_path) as f:
        tmp = json.load(f)

    # if archive folder doesn't exist, make it
    archive_dir = os.path.abspath(os.path.join(folder_path, 'archive'))
    if not path.exists(archive_dir):
        os.mkdir(archive_dir)

    # create time string
    now = datetime.now()
    current_time = now.strftime("%Y_%m_%d_%H_%M_%S")
    print(current_time)

    file_name = 'pizza_data_{}.json'.format(str(current_time))

    archive_path = os.path.abspath(
        os.path.join(archive_dir, file_name))
    print(archive_path)

    with open(archive_path, 'w') as outfile:
        json.dump(tmp, outfile)


def download_and_save_json(include_recipe=False, folder_path=None, json_path=None, overwrite_pizza_data=False):
    if path.exists(json_path) and overwrite_pizza_data == False:
        print('pizza_data already exists and is not being overwritten')
        return
    elif path.exists(json_path) and overwrite_pizza_data == True:
        print(
            'pizza_data already exists and is being overwritten. Old file saved in archive')
        archive_pizza_data(folder_path, json_path)

    if include_recipe:
        # currently limited to 100 items
        response = requests.get(
            'http://kitchen3.corp.serveautomation.com/rest/pizzas/')
    else:
        response = requests.get(
            'http://kitchen3.corp.serveautomation.com/api/rpc/summary/?format=json')
    print(response)
    with open(json_path, 'w') as outfile:
        json.dump(response.json(), outfile)


def create_new_csv():
    # load json
    with open('pizza_data.json') as f:
        pizza_data = json.load(f)

    # turn JSON to list of pizzas
    pizzas = pizza_data['pizzas']

    # sort ascending
    pizzas.sort(key=lambda x: x['id'])

    # manually enter list of IDs we care about
    ids = [30, 31, 142]

    # drop Ids that we don't care about
    # pizzas[:] = [x for x in pizzas if x['id'] in ids]

    # convert pizzas to dataframe
    df = pd.DataFrame(pizzas)

    # convert time strings to datetime
    df['cooking_at'] = pd.to_datetime(df['cooking_at'])
    df['completed_at'] = pd.to_datetime(df['completed_at'])

    # calculate difference
    df['cook_time'] = pd.to_timedelta(df['cooking_at'] - df['completed_at'])

    # convert to seconds
    df['cook_time'] = df['cook_time'].dt.seconds

    # save to csv
    df.to_csv('sorted.csv')


def update_test_log(log_path, out_df=None):

    book = load_workbook(log_path)
    writer = pd.ExcelWriter(log_path, engine='openpyxl')
    writer.book = book

    writer.sheets = dict((ws.title, ws) for ws in book.worksheets)
    out_df.to_excel(writer, "influx_data")
    writer.save()
    writer.close()


def load_pizza_data(json_path, id_list=None):

    # import the json to pandas
    # load json
    with open(json_path) as f:
        pizza_data = json.load(f)

    # turn JSON to list of pizzas
    pizzas = pizza_data['pizzas']

    # convert pizzas to dataframe
    pizza_df = pd.DataFrame(pizzas)
    pizza_df = pizza_df.set_index('id', drop=False)
    pizza_df = pizza_df.sort_index()
    keep_cols = ['id', 'cooking_at', 'completed_at',
                 'canceled_at', 'recipe_name', 'state', 'platform_id']
    pizza_df = pizza_df[keep_cols]

    # create a data frame that is filtered down from the pizza df
    pizza_df = pizza_df.loc[pizza_df['id'].isin(id_list)]

    # convert time strings to datetime and remove zone info
    if not pizza_df['cooking_at'].isnull().all():
        pizza_df['cooking_at'] = pd.to_datetime(
            pizza_df['cooking_at']).dt.tz_convert(
            'Etc/GMT+8')
        pizza_df['cooking_at'] = pizza_df['cooking_at'].dt.tz_localize(None)

    if not pizza_df['completed_at'].isnull().all():
        pizza_df['completed_at'] = pd.to_datetime(
            pizza_df['completed_at']).dt.tz_convert('Etc/GMT+8')
        pizza_df['completed_at'] = pizza_df['completed_at'].dt.tz_localize(
            None)

    if not pizza_df['canceled_at'].isnull().all():
        pizza_df['canceled_at'] = pd.to_datetime(
            pizza_df['canceled_at']).dt.tz_convert('Etc/GMT+8')
        pizza_df['canceled_at'] = pizza_df['canceled_at'].dt.tz_localize(None)

    # calculate difference
    pizza_df['cook_time'] = pd.to_timedelta(
        pizza_df['completed_at'] - pizza_df['cooking_at'])

    # convert to seconds
    pizza_df['cook_time'] = pizza_df['cook_time'].dt.seconds

    return pizza_df


def get_id_list(log_path):
    # load the test log sheet into pandas
    log_df = pd.read_excel(log_path, sheet_name='log', usecols=['id'])
    log_df = log_df.dropna(how='all')
    return log_df['id'].tolist()


def update_log(args):

    # compose paths from folder
    log_path = os.path.abspath(args.folder_path + r'\log.xlsx')
    json_path = os.path.abspath(args.folder_path + r'\pizza_data.json')

    # download pizza data and save
    download_and_save_json(
        include_recipe=args.include_recipe, folder_path=args.folder_path, json_path=json_path,
        overwrite_pizza_data=args.overwrite_pizza_data)

    # read excel file to get the pizza id list
    id_list = get_id_list(log_path)

    # load pizza data into pandas
    out_df = load_pizza_data(json_path=json_path, id_list=id_list)

    # working updates
    update_test_log(log_path, out_df=out_df)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--folder_path', type=str, default=None)
    parser.add_argument('--include_recipe', type=bool, default=False)
    parser.add_argument('--overwrite_pizza_data', type=bool, default=False)
    args = parser.parse_args()

    update_log(args)
