import dash
import dash_core_components as dcc
import dash_html_components as html
from dash.exceptions import PreventUpdate
import pandas as pd
import plotly.express as px
from datetime import date
import base64
import pprint

from DataTool.CookDay import CookDay

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']


image_filename = 'assets/output-collage.jpg'  # replace with your own image
encoded_image = base64.b64encode(open(image_filename, 'rb').read())

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)


def create_results_table():
    table_def = [
        {'display': 'Number of pizzas attempted:',
         'id': 'num-pizzas',
         'default': 0},
        {'display': 'Number of pizzas completed:',
         'id': 'num-pizzas-complete',
         'default': 0},
        {'display': 'Number of pizzas failed:',
         'id': 'num-pizzas-fail',
         'default': 0},
        {'display': 'Number of pizzas burned:',
         'id': 'num-pizzas-burned',
         'default': 0},
        {'display': 'Pizza ID list:',
         'id': 'pizza-id-list',
         'default': 0}
    ]
    left_div = []
    for item in table_def:
        left_div.append(html.Div(children=item['display']))

    right_div = []
    for item in table_def:
        right_div.append(html.Div(children=item['default'], id=item['id']))

    return left_div, right_div


app.layout = html.Div([

    # add navbar
    html.Div([]),

    # add title
    html.Div([
        html.H1(id='page-title', children='Cook Day Results')
    ]),

    # date picker
    html.Div([
        dcc.DatePickerSingle(
            id='date-picker',
            date=date.today()
        ),
        html.Button('Submit', id='date-submit', n_clicks=0),
        html.Div(id='output-date-picker')
    ]),

    # div for left right
    html.Div([
        # left - this is the results table
        html.Div([
            html.Div(create_results_table()[0], style={
                     'width': '70%', 'display': 'inline-block'}),
            html.Div(create_results_table()[1], style={
                     'width': '29%', 'float': 'right', 'display': 'inline-block'}),
        ], id='results-table',
            # table of cook day results. probably div for each
            style={'width': '30%', 'display': 'inline-block', 'border': '1px solid grey'}),

        # right
        html.Div([

            # pie chart top
            html.Div([
                html.Img(
                    id='pie-chart',
                    src="https://d2mvzyuse3lwjc.cloudfront.net/doc/en/UserGuide/images/2D_B_and_W_Pie_Chart/2D_B_W_Pie_Chart_1.png?v=83139")
            ]),

            # collage
            html.Div([
                html.Img(id='pizza-collage',
                         src='data:image/jpg;base64,{}'.format(
                             encoded_image.decode()),
                         alt='picture of pizza collage',
                         style={'max-width': '500px'})
            ]),
        ],
            style={'width': '69%', 'float': 'right', 'display': 'inline-block'}),
    ]),

    # selector for pizza ID
    html.Div([
        dcc.Dropdown(
            id='pizza-dropdown-options',
            # dynamically create options from cookday
            options=[
                {'label': 1, 'value': 1},
                {'label': 2, 'value': 2}
            ],
            value=[1],
            placeholder='select any number of pizzas',
            multi=True
        )
    ]),

    # pizza display
    html.Div([
        html.H2('pizza Display'),
        html.Div(id='pizza-displays',
                 style={'height': '100px', 'border': '1px grey solid'})
    ]),
])


@app.callback(
    dash.dependencies.Output('output-date-picker', 'children'),
    [dash.dependencies.Input('date-submit', 'n_clicks')],
    [dash.dependencies.State('date-picker', 'date')])
def update_output(n_clicks, date_val):
    string_prefix = 'You have selected: '
    if date_val is not None:
        print(date_val)
        date_object = date.fromisoformat(date_val)
        date_string = date_object.strftime('%Y.%m.%d')
        print(date_string)
        string_prefix = string_prefix + date_string
    if len(string_prefix) == len('You have selected: '):
        return 'Select a date to see it displayed here'
    else:
        return string_prefix


@app.callback(
    dash.dependencies.Output('num-pizzas', 'children'),
    dash.dependencies.Output('num-pizzas-complete', 'children'),
    dash.dependencies.Output('num-pizzas-fail', 'children'),
    dash.dependencies.Output('num-pizzas-burned', 'children'),
    dash.dependencies.Output('pizza-id-list', 'children'),
    [dash.dependencies.Input('date-submit', 'n_clicks')],
    [dash.dependencies.State('date-picker', 'date')]
)
def pull_data(n_clicks, date_value):
    num_pizzas = 'N/A'
    num_pizzas_complete = 'N/A'
    num_pizzas_fail = 'N/A'
    num_pizzas_burned = 'N/A'
    pizza_id_list = 'N/A'
    print('pulling data')
    date_object = date.fromisoformat(date_value)
    date_string = date_object.strftime('%Y.%m.%d')
    try:
        cook_day = CookDay(date=date_string,
                           image_folder=r'C:\Users\ArikJenkins\Serve Automation\Serve Main - 13 Testing\Logs\20210408')
    except:
        print('no data')
        return num_pizzas, num_pizzas_complete, num_pizzas_fail, num_pizzas_burned, pizza_id_list
    print('done with data pull')
    pizza_id_list = cook_day.pizza_df['id']
    return num_pizzas, num_pizzas_complete, num_pizzas_fail, num_pizzas_burned, pizza_id_list


@app.callback(
    dash.dependencies.Output("pizza-dropdown-options", "options"),
    [dash.dependencies.Input("pizza-id-list", "children")],
)
def update_pizza_dropdown_options(pizza_list):
    if not pizza_list:
        raise PreventUpdate
    # needs to return list of option objects
    # options = []
    # for pizza in pizza_list:
    #     options += [{'label': pizza, 'value': pizza}]
    options = [{'label': x, 'value': x} for x in pizza_list]
    return options


if __name__ == '__main__':
    app.run_server(debug=True)
