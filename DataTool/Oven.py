import pandas as pd
import matplotlib.pyplot as plt


from .DataRun import DataRun


OLD_TC_CONFIG = {
    'oven-1': {
        'griddle': [],
        'outer_broiler': [],
        'inner_broiler': [],
        'air': [{'channel': 2,
                 'label': 'Air over broilers',
                 'color': 'orange'}]
    },
    'oven-2': {
        'griddle': [],
        'outer_broiler': [{'channel': 8,
                           'label': 'extra outer broiler',
                           'color': 'orange'}],
        'inner_broiler': [],
        'air': [{'channel': 2,
                 'label': 'Air over broilers',
                 'color': 'orange'},
                {'channel': 3,
                 'label': 'Air back left',
                 'color': 'green'},
                {'channel': 7,
                 'label': 'Air at door',
                 'color': 'blue'}]
    },
    'oven-3': {
        'griddle': [{'channel': 5,
                     'label': 'griddle center',
                     'color': 'orange'},
                    {'channel': 1,
                     'label': 'griddle 1',
                     'color': 'y'},
                    {'channel': 8,
                     'label': 'griddle 3',
                     'color': 'c'},
                    {'channel': 9,
                     'label': 'griddle 4',
                     'color': 'b'}],
        'outer_broiler': [],
        'inner_broiler': [],
        'air': [{'channel': 2,
                 'label': 'Air over broilers',
                 'color': 'orange'},
                {'channel': 3,
                 'label': 'Air back left',
                 'color': 'green'},
                {'channel': 7,
                 'label': 'Air at door',
                 'color': 'blue'}]
    },
    'oven-4': {
        'griddle': [],
        'outer_broiler': [],
        'inner_broiler': [],
        'air': [{'channel': 2,
                 'label': 'Air over broilers',
                 'color': 'orange'},
                {'channel': 3,
                 'label': 'Oven 4 exhaust',
                 'color': 'green'},
                {'channel': 4,
                 'label': 'chimney stack temp',
                 'color': 'blue'}]
    }
}

OLD_TC_CONFIG = {
    'oven-1': {
        'griddle': [],
        'outer_broiler': [],
        'inner_broiler': [],
        'air': [{'channel': 2,
                 'label': 'Air over broilers',
                 'color': 'orange'}]
    },
    'oven-2': {
        'griddle': [],
        'outer_broiler': [{'channel': 8,
                           'label': 'extra outer broiler',
                           'color': 'orange'}],
        'inner_broiler': [],
        'air': [{'channel': 2,
                 'label': 'Air over broilers',
                 'color': 'orange'},
                {'channel': 3,
                 'label': 'Air back left',
                 'color': 'green'},
                {'channel': 7,
                 'label': 'Air at door',
                 'color': 'blue'}]
    },
    'oven-3': {
        'griddle': [{'channel': 5,
                     'label': 'griddle center',
                     'color': 'orange'},
                    {'channel': 1,
                     'label': 'griddle 1',
                     'color': 'y'},
                    {'channel': 8,
                     'label': 'griddle 3',
                     'color': 'c'},
                    {'channel': 9,
                     'label': 'griddle 4',
                     'color': 'b'}],
        'outer_broiler': [],
        'inner_broiler': [],
        'air': [{'channel': 2,
                 'label': 'Air over broilers',
                 'color': 'orange'},
                {'channel': 3,
                 'label': 'Air back left',
                 'color': 'green'},
                {'channel': 7,
                 'label': 'Air at door',
                 'color': 'blue'}]
    },
    'oven-4': {
        'griddle': [],
        'outer_broiler': [],
        'inner_broiler': [],
        'air': [{'channel': 2,
                 'label': 'Air over broilers',
                 'color': 'orange'},
                {'channel': 3,
                 'label': 'Oven 4 exhaust',
                 'color': 'green'},
                {'channel': 4,
                 'label': 'chimney stack temp',
                 'color': 'blue'}]
    }
}

TC_CONFIG = OLD_TC_CONFIG
# for thing in extra_tc_config[station][field]:
#     plt.plot(thing['channel'], label=thing['label'], c=thing['color'])


class Oven():

    def __init__(self, station, start_time, end_time):
        self.station = station
        self.fields = []
        self.df = None
        self.start_time = start_time
        self.end_time = end_time
        self.generate_field_list()
        self.load_data_from_influx()
        self.create_wait_list()

    def load_data_from_excel(data_path):
        df = pd.read_excel(data_path)
        df['Time'] = pd.to_datetime(df['Time']).dt.tz_convert('Etc/GMT+8')
        df['Time'] = df['Time'].dt.tz_localize(None)
        return df

    def generate_field_list(self):
        fields = ['Time', 'pizza_id', 'state']
        areas = ['griddle', 'inner_broiler', 'outer_broiler']
        for i in range(1, 10):
            fields.append('temperatures.ch{}_temp_c'.format(str(i)))

        # TODO: update the sensors we need here
        for area in areas:
            fields.append(area + '.temp_a_c')
            fields.append(area + '.temp_b_c')
            fields.append(area + '.cmd_temp_c')
            fields.append(area + '.temperature_tolerance_lower_bound_c')
            fields.append(area + '.temperature_tolerance_upper_bound_c')
            fields.append(area + '.valve.cmd_pressure_psi')
            fields.append(area + '.valve.pressure_psi')
            fields.append(area + '.igniter.flame_present')

        fields.append('temperatures.air_inlet_temp_c')
        fields.append('temperatures.air_exhaust_temp_c')
        fields.append('temperatures.air_chamber_temp_c')
        fields.append('temperatures.air_door_temp_c')
        fields.append('temperatures.air_rear_temp_c')
        fields.append('temperatures.air_broiler_temp_c')

        self.fields = fields

    def load_data_from_influx(self):
        data_run = DataRun(self.fields,
                           self.start_time,
                           self.end_time,
                           station=self.station)
        self.data_run = data_run
        self.df = data_run.df

    def create_wait_list(self):
        # find the times when we are waiting in preconditions. state==3
        # df['state'] = df['state'].astype('int')
        state_bools = self.df['state'] == 'wait_for_entry_preconditions'
        idx_list = []
        last_val = False
        for idx, val in enumerate(state_bools):
            # if last was false and this is true its a rising edge
            if not last_val and val:
                start_idx = idx
            # if last was true and this is false then falling edge
            if last_val and not val:
                idx_list.append((start_idx, idx))
            last_val = val
        self.idx_list = idx_list

    # calculate on time of each oven during a time period and save to a new channel
    # note that time itervals are not always static coming from our data stream
    # so cannot just increment an index, need to actually add the time
    # outer_broiler_on_time
    # inner_broiler_on_time
    # griddle_on_time
    # should we use on state, flame_present, or anytime pressure is commanded to above zero?
    def compute_on_times(self):
        # Loop through burners and anytime the pressure was on, add the time
        # difference to a new channel
        areas = ['griddle', 'inner_broiler', 'outer_broiler']
        for area in areas:
            data = self.df[area + '.valve.cmd_pressure_psi']
            # Do i need to convert time to seconds here?
            time = self.df['Time']

            # simple forward sum
            new_data = []
            data_len = len(data)
            for i in range(data_len - 1):
                if data[i] >= 0.1:
                    new_data[i] = time[i+1] - time[i]
                else:
                    if i == 0:
                        new_data[i] = 0
                    else:
                        new_data[i] = new_data[i - 1]
                        
            # add one more point to keep data size the same
            new_data[data_len] = new_data[data_len - 1]

            # save the result back into the data frame.
            self.df[area + '_on_time'] = new_data


    # Integrate pressure over time during all time and save to a new channel. Can later zero by beginning of time period
    # outer_broiler_pressure_integral
    # inner_broiler_pressure_integral
    # griddle_pressure_integral
    def compute_pressure_integrals(self):
        areas = ['griddle', 'inner_broiler', 'outer_broiler']
        for area in areas:
            data = self.df[area + '.valve.pressure_psi']
            # Do i need to convert time to seconds here?
            time = self.df['Time']

            # use numpy to integrate

            # simple forward sum
            new_data = []
            data_len = len(data)
            for i in range(data_len - 1):
                new_data[i] = data[i] * (time[i+1] - time[i])
            # add one more point to keep data size the same
            new_data[data_len] = new_data[data_len - 1]
            
            # save the result back into the data frame.
            self.df[area + '_pressure_integral'] = new_data


    def plot_wait_windows(self):
        for entry in self.idx_list:
            plt.axvspan(self.df.index[entry[0]], self.df.index[entry[1]],
                        alpha=0.3, color='red')

    def add_oven_subplot(self, field, color, include_pressure, root_ax, n, k):
        if root_ax is None:
            ax = plt.subplot(n, 1, k)
        else:
            ax = plt.subplot(n, 1, k, sharex=root_ax)

        plt.plot(self.df[field+'.cmd_temp_c'], c='k', linestyle='--')
        low_lim = self.df[field+'.cmd_temp_c'] + self.df[field + '.temperature_tolerance_lower_bound_c']
        high_lim = self.df[field+'.cmd_temp_c'] + self.df[field + '.temperature_tolerance_upper_bound_c']
        plt.plot(high_lim, c='r', linestyle='--')
        plt.plot(low_lim, c='r', linestyle='--')
        plt.plot(self.df[field+'.temp_a_c'], c=color, linestyle='-', label=field + ' temperature cha')
        plt.plot(self.df[field+'.temp_b_c'], c=color, linestyle='--', label=field + ' temperature chb')
        plt.grid(axis='y', which='major')
        # plt.ylim(low_lim[0] * 0.9, high_lim[0] * 1.1)
        # if oven-3 add the extra TCs

        # for item in EXTRA_TC_CONFIG[self.station][field]:
        #     chan_num = item['channel']
        #     chan = f'temperatures.ch{chan_num}_temp_c'
        #     plt.plot(self.df[chan],
        #              c=item['color'], label=item['label'])

        ax.set_ylabel('Temp [c]', c=color)
        ax.legend(loc='upper left')
        # self.plot_wait_windows()
        if include_pressure:
            ax2 = ax.twinx()
            ax2.plot(self.df[field+'.valve.pressure_psi'], c='k',
                     linestyle='--', label=field+' Pressure')
            ax2.set_ylabel('Pressure [psi]')
            ax2.legend(loc='upper right')

        return ax

    def make_plots(self, show_pressure):
        plt.figure()
        n = 6
        ax1 = self.add_oven_subplot(
            'griddle', (0, 0.5, 0.1), show_pressure, None, n, 1)

        ax2 = self.add_oven_subplot(
            'outer_broiler', (0.7, 0.4, 0), show_pressure, ax1, n, 2)

        ax3 = self.add_oven_subplot(
            'inner_broiler', (0, 0.3, 0.8), show_pressure, ax1, n, 3)

        # air temps
        ax4 = plt.subplot(n, 1, 4, sharex=ax1)
        # for item in EXTRA_TC_CONFIG[self.station]['air']:
        #     chan_num = item['channel']
        #     chan = f'temperatures.ch{chan_num}_temp_c'
        #     plt.plot(self.df[chan],
        #              c=item['color'], label=item['label'])
        plt.plot(self.df['temperatures.air_inlet_temp_c'],c='k', label='inlet')
        plt.plot(self.df['temperatures.air_exhaust_temp_c'],c='g', label='exhaust')
        plt.plot(self.df['temperatures.air_chamber_temp_c'],c='b', label='chamber')
        plt.plot(self.df['temperatures.air_door_temp_c'],c='r', label='door')
        plt.plot(self.df['temperatures.air_rear_temp_c'],c='y', label='rear')
        plt.plot(self.df['temperatures.air_broiler_temp_c'],c='c', label='broiler')
        ax4.set_ylabel('Temp [c]', c='k')
        self.plot_wait_windows()
        plt.legend(loc='upper left')

        # pizza ID
        ax5 = plt.subplot(n, 1, 5, sharex=ax1)
        plt.plot(self.df['pizza_id'], c='k', linestyle='-', label='Pizza Id')
        self.plot_wait_windows()
        plt.legend(loc='upper right')

        # state
        ax6 = plt.subplot(n, 1, 6, sharex=ax1)
        plt.plot(self.df['state'], c='k', linestyle='-', label='State')
        self.plot_wait_windows()
        plt.legend(loc='upper right')

        plt.subplots_adjust(hspace=0.0)
        plt.suptitle(self.station + ' Data')
        plt.show()

    def save_oven_data(self, folder_path):
        self.data_run.save_data(folder_path)
