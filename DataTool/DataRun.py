from socket import error
import pandas as pd
import time
import matplotlib.pyplot as plt
from influxdb import DataFrameClient
import pprint
import os
from os import path
from datetime import datetime

from .data_helpers import convert_to_epoch, TIME_ZONE_STRING


class DataRun():
    '''
    This class grabs data from influx with given fields and in a time range
    and returns a pandas database.
    Potentially additions:
    - downsampling
    - limiting data size?
    - add checks on the data table to make sure that all fields entered are in 
        the resultant dataframe and notify the user if they are not.

    inputs:
    - fields (array): list of strings that are the field names for the query
    - start_time (string): date time string in the format of yyyy.mm.dd HH:MM:SS
    - end_time (string): date time string in the format of yyyy.mm.dd HH:MM:SS
    - station (string) [optional]: an additional tag filter to narrow results

    returns:
    - an object with the following attributes:
        - df (pandas dataframe): results of the query
        - station (string): the station tag used for the query
        - query_string (string): the full query string
    '''

    # currently no auth
    USER = None
    PASSWORD = None

    # database is the overall datasource with multiples tables
    # print(client.get_list_database())

    # measurements are like tables. databases have multiple tables
    # The only measurement we really care about is controller. It contains
    # the timeseries data
    MEASUREMENT = "controller"

    # tags are optional parts of the data that are useful for grouping like data
    # we use the station tag as our only grouping.
    STATION_OPTIONS = ['aggregator', 'dough-flipper', 'dough-flipper-vision', 'dough-press', 'dough-storage', 'flasher', 'launcher-gantry', 'launcher-gantry-vision',
                       'pepperoni', 'power-distribution', 'sauce-n-cheese', 'sequencer', 'serve-conveyor', 'serve-conveyor-labeler', 'oven-1', 'oven-2', 'oven-3', 'oven-4']

    # fields are the column names of the table
    SOME_FIELDS = ['Time', 'griddle.temp_c', 'griddle.cmd_temp_c', 'outer_broiler.temp_c',
                   'outer_broiler.cmd_temp_c', 'inner_broiler.temp_c',
                   'inner_broiler.cmd_temp_c', 'pizza_id', 'state', 'griddle.valve.pressure_psi',
                   'griddle.valve.cmd_pressure_psi', 'outer_broiler.valve.pressure_psi', 'outer_broiler.valve.cmd_pressure_psi',
                   'inner_broiler.valve.pressure_psi', 'inner_broiler.valve.cmd_pressure_psi']

    station = None
    # station has to be in single quotes
    sample_station = "'oven-3'"
    field_string = ""
    start_time = None
    end_time = None

    kitchen2_live_config = {
        "host": 'kitchen2.corp.serveautomation.com',
        "port": 8086,
        "ssl": False,
        "verify_ssl": False,
        "path": '',
        "database": 'kitchen',
    }

    kitchen3_live_config = {
        "host": 'kitchen3.corp.serveautomation.com',
        "port": 8086,
        "ssl": False,
        "verify_ssl": False,
        "path": '',
        "database": 'kitchen',
    }
    
    kitchen_data_config = {
        "host": 'kitchen-data.corp.serveautomation.com',
        "port": 443,
        "ssl": True,
        "verify_ssl": True,
        "path": '/influxdb',
        "database": 'kitchen3',
    }

    active_config = kitchen_data_config

    def __init__(self, fields, start_time, end_time, station=None):
        self.client = DataFrameClient(**DataRun.active_config)
        self.client.switch_database(DataRun.active_config['database'])

        self.start_time = convert_to_epoch(start_time)
        self.end_time = convert_to_epoch(end_time)
        # print('start time', self.start_time)

        if station is not None:
            self.station = "'{}'".format(station)
        else:
            self.station = station
        self.fields = fields
        self.field_string = self._generate_field_string()
        self.df = pd.DataFrame({})

        # self.time = None
        # self.data = None

        # build the query string
        if self.station is not None:
            self.query_string = 'SELECT {} FROM {} WHERE "station"={} AND time >= {}s and time <= {}s'.format(
                self.field_string, DataRun.MEASUREMENT, self.station, self.start_time, self.end_time)
        else:
            self.query_string = 'SELECT {} FROM {} WHERE time >= {}s and time <= {}s'.format(
                self.field_string, DataRun.MEASUREMENT, self.start_time, self.end_time)

        # this is a collections.defaultdict object
        results = self.client.query(self.query_string)
        # controller is the key of the dataframe that we want.
        try:
            self.df = results['controller']
            self._localize_time()
            # self.time = self.df.index
            # self.data = self.df[self.name]
        except KeyError:
            print('No data found for the target time range')

    def _generate_field_string(self):
        for channel in self.fields:
            self.field_string += '"{}", '.format(channel)
        return self.field_string[:-2]

    def _validate_fields(self):
        for field in self.fields:
            if field not in self.df.columns:
                print('Error pulling {} from influx. No data present'.format(field))

    def _localize_time(self):
        self.df['Time'] = self.df.index
        self.df['Time'] = pd.to_datetime(
            self.df['Time']).dt.tz_convert(TIME_ZONE_STRING)
        self.df['Time'] = self.df['Time'].dt.tz_localize(None)
        self.df.index = self.df['Time']

    def show_tags(self):
        # prints out all of the tags in our database
        tag_query = 'SHOW TAG VALUES FROM "controller" WITH KEY = "station"'
        results = self.client.query(tag_query)
        pp = pprint.PrettyPrinter(width=100, compact=True)
        pp.pprint(results.raw)

    def show_fields(self):
        # prints out all of the fields in our database
        # TODO add option to downsample fields by station?? does it work?
        tag_query = 'SHOW FIELD KEYS'
        if not self.station is None:
            tag_query = 'SHOW FIELD KEYS WHERE "station"={}'.format(
                self.station)
        results = self.client.query(tag_query)
        pp = pprint.PrettyPrinter(width=100, compact=True)
        pp.pprint(results.raw)

    def save_data(self, folder_path):
        # create the data folder if it doesn't exist
        data_dir = os.path.abspath(os.path.join(folder_path, 'saved_data'))
        if not path.exists(data_dir):
            os.mkdir(data_dir)

        # create time string
        now = datetime.now()
        current_time = now.strftime("%Y_%m_%d_%H_%M_%S")
        print(current_time)

        if self.station is not None:
            stat = self.station[1:-1]
            stat = stat.replace('-', '_')

        file_name = '{}_data_{}.csv'.format(stat, str(current_time))

        save_to = os.path.join(data_dir, file_name)
        self.df.to_csv(save_to)


if __name__ == '__main__':

    data_run = DataRun(fields=DataRun.SOME_FIELDS,
                       start_time='2021.03.08 10:00:00',
                       end_time='2021.03.08 14:00:00',
                       station='oven-3')
    data_run.show_fields()
    print(data_run.df.head(5))
