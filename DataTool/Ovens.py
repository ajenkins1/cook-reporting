from .Oven import Oven
import numpy as np
import matplotlib.pyplot as plt
from .data_helpers import butter_lowpass_filter

class Ovens():

    def __init__(self, start_time, end_time):
        self.ovens = []
        for i in range(1, 5):
            oven = Oven(f'oven-{i}', start_time, end_time)
            print(f'plotting data for oven {i}')
            # oven.make_plots(False)
            self.ovens.append(oven)

    def plot_all_ovens(self):
        for oven in self.ovens:
            oven.make_plots(False)

    def plot_air_comparison(self):
        colors = ['k', 'b', 'r', 'g']
        thing = ['inlet', 'exhaust', 'door', 'rear', 'broiler']
        for chan_name in thing:
            plt.figure()
            for j, oven in enumerate(self.ovens):
                plt.plot(oven.df['Time'], np.abs(butter_lowpass_filter(
                    oven.df['temperatures.air_'+chan_name+'_temp_c'], 0.001, 5, 2)), c=colors[j], label=f'oven {j+1} {chan_name} TC')
            plt.title(chan_name)
            plt.legend(loc='upper right')
            plt.ylabel('temp [c]')
            plt.show()