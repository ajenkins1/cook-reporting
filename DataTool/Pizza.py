import glob
from .DataRun import DataRun
import matplotlib.pyplot as plt

# TODO look at leveraging the vision software to get burn percent and cropped images.


class Pizza():
    '''
    Class that represents a single pizza that has gone through the machine

    Inputs:
    - 
    '''

    SETTINGS = {}

    id = None
    recipe = None
    img_paths = {}
    start_time = None
    end_time = None
    recipe = None
    result = None  # completed / canceled
    is_burned = None
    canceled_time = None
    canceled_station = None
    oven_id = None
    # calculating timing here
    # enter will be the beginning of the entry routine of that station
    # exit will be the beginning of the exit routine of that station.
    TIMING_SCHEMA = {
        'dough-storage': {
            'enter': None,
            'exit': None
        },
        'dough-flipper': {
            'enter': None,
            'exit': None
        },
        'dough-press': {
            'enter': None,
            'press_start': None,
            'press_finish': None,
            'exit': None
        },
        'sauce-n-cheese': {
            'enter': None,
            'sauce_start': None,
            'sauce_finish': None,
            'cheese_start': None,
            'cheese_finish': None,
            'exit': None
        },
        'pepperoni': {
            'enter': None,
            'exit': None
        },
        'launcher': {
            'enter': None,
            'at_oven_height': None,
            'exit': None
        },
        'oven': {
            'enter': None,
            'door_open_enter': None,
            'door_close_enter': None,
            'door_open_exit': None,
            'door_close_exit': None,
            'exit': None
        }
    }

    # def __init__(self, id, recipe, start_time, end_time, result, canceled_time, image_folder, oven_id):
    def __init__(self, kitchen_data, image_folder, oven_id):

        self.oven_id = oven_id
        self.timing = Pizza.TIMING_SCHEMA

        self.is_burned = None
        self.canceled_station = None

        self.image_paths = {}
        self._unpack_input()
        self._get_station_timing()
        # find the path to this pizzas images
        self._find_img_paths(image_folder)

    def _unpack_input(self, kitchen_data):
        self.id = kitchen_data['id']
        self.recipe = kitchen_data['recipe_name']
        self.result = kitchen_data['state']

        self.canceled_time = kitchen_data['canceled_time']
        # start time should always be cook_at
        self.start_time = kitchen_data['cooking_at']
        # end time should be the later of completed_at or canceled_at whichever isnt null
        # end time is useful for making sure we pull all of the data
        # TODO I'm not handling null vs None upstream and should be sanitizing times.
        # TODO currently a case where failed pizzas do not receive a canceled_at time.
        # need to add a third case where we just choose an end time if none is provided.
        self.end_time = kitchen_data['completed_at']
        if kitchen_data['canceled_at'] != None:
            self.end_time = kitchen_data['canceled_at']
        

    def _find_img_paths(self, folder):
        # In the case of multiple paths, just takes the first path found
        # TODO check output of glob and figure out how to take only one path in case of multiple

        # flipper vision path
        self.image_paths['flipper'] = glob.glob(
            folder+'*flipper*-{}-entry*.jpg'.format(self.id))

        # post_cook path
        self.image_paths['pre-cook'] = glob.glob(
            folder+'*launcher*-{}-entry*.jpg'.format(self.id))

        # pre-cook path
        self.image_paths['post-cook'] = glob.glob(
            folder+'*launcher*-{}-exit*.jpg'.format(self.id))

    def _get_station_timing(self):
        dough_storage = DataRun(
            ['pizza_id', 'state'], self.start_time, self.end_time, 'dough-storage')
        print('dough storage')
        # print(dough_storage.df.head(10))
        flipper = DataRun(['pizza_id', 'state'],
                          self.start_time, self.end_time, 'dough-flipper')
        print('dough-flipper')
        # print(flipper.df.head(10))

    def plot_temps(self):
        if self.result == 'completed':
            griddle_data = DataRun(
                'griddle.temp_c', start_time=self.start_time,
                end_time=self.end_time, station=self.oven_id)
            if griddle_data.df['griddle.temp_c'] is not None:
                plt.figure()
                plt.plot(griddle_data.df['Time'],
                         griddle_data.df['griddle.temp_c'])
                plt.title('Pizza: {} | Oven: {} | Griddle Temp'.format(
                    self.id, self.oven_id))
                plt.show()
        else:
            print('pizza was unsuccessful')
