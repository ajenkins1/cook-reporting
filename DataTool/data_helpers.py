import time
from scipy.signal import butter, filtfilt


def convert_to_epoch(timestamp=None, pattern=None):
    # converts a timestamp string to seconds since epoch
    # date_time = '2021.03.08 10:00:00'
    if pattern is None:
        pattern = '%Y.%m.%d %H:%M:%S'
    try:
        epoch = int(time.mktime(time.strptime(timestamp, pattern)))
    except TypeError:
        print('Timestamp given to convert_to_epoch wasnt a string!!')
        return 0
    return epoch


# +7 when in spring daylight savings. +8 fall and winter...
TIME_ZONE_STRING = 'Etc/GMT+7'


def butter_lowpass_filter(data, cutoff, fs, order):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    # Get the filter coefficients
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    y = filtfilt(b, a, data)
    return y
