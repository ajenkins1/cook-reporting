from socket import error
import pandas as pd
import time
import matplotlib.pyplot as plt
from influxdb import InfluxDBClient, DataFrameClient
import pprint
import socket

from .data_helpers import convert_to_epoch, TIME_ZONE_STRING


class InfluxHelper():
    '''
    This class grabs data from influx with given fields and in a time range
    and returns a pandas database.
    Potentially additions:
    - downsampling
    - limiting data size?
    - add checks on the data table to make sure that all fields entered are in 
        the resultant dataframe and notify the user if they are not.

    inputs:
    - fields (array): list of strings that are the field names for the query
    - start_time (string): date time string in the format of yyyy.mm.dd HH:MM:SS
    - end_time (string): date time string in the format of yyyy.mm.dd HH:MM:SS
    - station (string) [optional]: an additional tag filter to narrow results

    outputs:
     - df (pandas dataframe): results of the query
    '''

    URL = 'kitchen2.corp.serveautomation.com'
    PORT = 8086

    # currently no auth
    USER = None
    PASSWORD = None

    # database is the overall datasource with multiples tables
    # print(client.get_list_database())
    DATABASE = 'kitchen'

    # measurements are like tables. databases have multiple tables
    # The only measurement we really care about is controller. It contains
    # the timeseries data
    MEASUREMENT = "controller"

    # tags are optional parts of the data that are useful for grouping like data
    # we use the station tag as our only grouping.
    STATION_OPTIONS = ['aggregator', 'dough-flipper', 'dough-flipper-vision', 'dough-press', 'dough-storage', 'flasher', 'launcher-gantry', 'launcher-gantry-vision',
                       'pepperoni', 'power-distribution', 'sauce-n-cheese', 'sequencer', 'serve-conveyor', 'serve-conveyor-labeler', 'oven-1', 'oven-2', 'oven-3', 'oven-4']

    # fields are the column names of the table
    SOME_FIELDS = ['Time', 'griddle.temp_c', 'griddle.cmd_temp_c', 'outer_broiler.temp_c',
                   'outer_broiler.cmd_temp_c', 'inner_broiler.temp_c',
                   'inner_broiler.cmd_temp_c', 'pizza_id', 'state', 'griddle.valve.pressure_psi',
                   'griddle.valve.cmd_pressure_psi', 'outer_broiler.valve.pressure_psi', 'outer_broiler.valve.cmd_pressure_psi',
                   'inner_broiler.valve.pressure_psi', 'inner_broiler.valve.cmd_pressure_psi']

    station = None
    # station has to be in single quotes
    sample_station = "'oven-3'"
    field_string = ""
    start_time = None
    end_time = None

    def __init__(self, fields, start_time, end_time, station=None):
        self.client = DataFrameClient(
            host=InfluxHelper.URL, port=InfluxHelper.PORT)
        self.client.switch_database(InfluxHelper.DATABASE)

        self.start_time = convert_to_epoch(start_time)
        self.end_time = convert_to_epoch(end_time)

        self.fields = fields
        self.field_string = self._generate_field_string()
        self.station = "'{}'".format(station)

        # build the query string
        if self.station is not None:
            q_string = 'SELECT {} FROM {} WHERE "station"={} AND time >= {}s and time <= {}s'.format(
                self.field_string, InfluxHelper.MEASUREMENT, self.station, self.start_time, self.end_time)
        else:
            q_string = 'SELECT {} FROM {} WHERE time >= {}s and time <= {}s'.format(
                self.field_string, InfluxHelper.MEASUREMENT, self.start_time, self.end_time)

        # this is a collections.defaultdict object
        results = self.client.query(q_string)
        # controller is the key of the dataframe that we want.
        try:
            self.df = results['controller']
            self._localize_time()
        except KeyError:
            print('No data found for the target time range')

    def _generate_field_string(self):

        for channel in self.fields:
            self.field_string += '"{}", '.format(channel)
        return self.field_string[:-2]

    def _validate_fields(self):
        for field in self.fields:
            if field not in self.df.columns:
                print('Error pulling {} from influx. No data present'.format(field))

    def _localize_time(self):
        self.df['Time'] = self.df.index
        self.df['Time'] = pd.to_datetime(
            self.df['Time']).dt.tz_convert(TIME_ZONE_STRING)
        self.df['Time'] = self.df['Time'].dt.tz_localize(None)
        self.df.index = self.df['Time']

    def show_tags(self):
        # prints out all of the tags in our database
        tag_query = 'SHOW TAG VALUES FROM "controller" WITH KEY = "station"'
        results = self.client.query(tag_query)
        pp = pprint.PrettyPrinter(width=100, compact=True)
        pp.pprint(results.raw)

    def show_fields(self):
        # prints out all of the fields in our database
        # TODO add option to downsample fields by station?? does it work?
        tag_query = 'SHOW FIELD KEYS'
        if self.station is not None:
            tag_query = 'SHOW FIELD KEYS WHERE "station"={}'.format(
                self.station)
        results = self.client.query(tag_query)
        pp = pprint.PrettyPrinter(width=100, compact=True)
        pp.pprint(results.raw)


if __name__ == '__main__':

    influx_helper = InfluxHelper(fields=InfluxHelper.SOME_FIELDS,
                                 start_time='2021.03.08 10:00:00',
                                 end_time='2021.03.08 14:00:00',
                                 station='oven-3')
    influx_helper.print_head(15)
    influx_helper.plot_data()
