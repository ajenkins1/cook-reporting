from numpy.lib.type_check import imag
import requests
import pandas as pd
import datetime as dt
import pprint
import json
import os
from os import path
import glob
import shutil

from .Pizza import Pizza
from .DataRun import DataRun
from .data_helpers import convert_to_epoch

# TODO unify method of time keeping. Probably should use datetime and convert in
# situ if it must be something else.
# TODO sanitize inputs. there are a few areas where pulling data has nulls or
# other unnaceptable data types.


class CookDay():
    '''
    This class is a data container for all of the results of a given cook day.
    It will find all pizzas cooked on the day in question and pull their data.


    inputs:
    - date (string): date string in the form of 'yyyy.mm.dd'
    - image_folder (string): path to the folder where pizza images are stored.

    returns:
    an object with the following notable attributes:
        - pizzas (array): Array of Pizza objects
        - num_pizzas (int): Number of pizzas cooked that day
        - num_success (int): Number of pizzas with "completed" as the status
        - num_fail (int): Number of pizzas with "cancelled" as the status
        - average_takt (float): the total cook time divided by number of pizzas
        - average_cook duration (float): the average of each pizza's cook time

    and the following methods:
        - generate_pie_chart: creates a pass / fail pie chart
        - generate_pizza_collage: creates the pizza collage for the day
    '''

    # takes in date and indentifies first and last pizza run from that day
    pizzas = None
    num_pizzas = None
    num_success = None
    num_fail = None
    num_burned = None
    average_takt = None
    average_cook_duration = None

    day_start = None
    day_end = None

    def __init__(self, date):
        self.date = date
        self._make_img_folder()

        self.pizza_df = None

        self.day_start = convert_to_epoch(self.date, '%Y.%m.%d')
        self.day_end = convert_to_epoch(self.date, '%Y.%m.%d') + 24*60*60

        # find ids on this date
        # self._pull_postgres_data()
        # filtering pizzas by date is likely not necessary anymore because the
        # postgres data now is only for one date.
        # self._filter_by_date()
        self.pizzas = []
        self.oven_pizza_ids = self.get_oven_pizza_ids()

        # self._create_pizzas()

    def _make_img_folder(self):
        # base_folder = r'C:\Users\ArikJenkins\Serve Automation Inc\Serve Main - 13 Testing\Logs'
        base_folder = r'C:\Users\ArikJenkins\Serve Automation Inc\Serve Main - Engineering\13 Testing\Logs'
        date_str = self.date.replace('.','')
        date_w_dash = self.date.replace('.', '-')
        self.image_folder =  os.path.abspath(os.path.join(base_folder, date_str, 'images', 'vision', date_w_dash))

    def _convert_column_date(self, pizza_df, chan):
        try:
            pizza_df[chan] = pd.to_datetime(
                pizza_df[chan]).dt.tz_convert('Etc/GMT+7')
            pizza_df[chan] = pizza_df[chan].dt.tz_localize(None)
            pizza_df[chan] = pizza_df[chan].dt.strftime(
                '%Y.%m.%d %H:%M:%S')
            pizza_df[chan] = pizza_df[chan].astype('string')
        except TypeError:
            return pizza_df
        return pizza_df

    def _pull_postgres_data(self):
        # right now, I'm backing up the postgres database manually. Should hit
        # the backup location instead of the main server
        # response = requests.get(
        #     'http://kitchen2.corp.serveautomation.com/rpc/summary/?format=json')
        # data = response.json()
        base_path = r"C:\Users\ArikJenkins\Serve Automation\Serve Main - Engineering\13 Testing\Logs\postgres_data"
        # date = '20210308'
        date = self.date.replace('.', '')

        try:
            with open(base_path + '\\' + date + '.json') as f:
                data = json.load(f)
        except FileNotFoundError:
            print('postgres data not found.')
        # this is a list of pizzas with parameters id, cooking_at, completed_at
        # canceled_at, recipe_name, state
        pizzas = data['pizzas']

        # convert pizzas to dataframe
        pizza_df = pd.DataFrame(pizzas)
        pizza_df = pizza_df.set_index('id', drop=False)
        pizza_df = pizza_df.sort_index()
        keep_cols = ['id', 'queued_at', 'cooking_at', 'completed_at',
                     'canceled_at', 'recipe_name', 'state']
        pizza_df = pizza_df[keep_cols]

        # create a data frame that is filtered down from the pizza df by date
        # pizza_df = pizza_df.loc[pizza_df['id'].isin(id_list)]

        # convert time strings to datetime and remove zone info
        pizza_df = self._convert_column_date(pizza_df, 'queued_at')
        pizza_df = self._convert_column_date(pizza_df, 'cooking_at')
        pizza_df = self._convert_column_date(pizza_df, 'completed_at')
        pizza_df = self._convert_column_date(pizza_df, 'canceled_at')

        self.pizza_df = pizza_df
        # print(self.pizza_df.head(5))

    def _filter_by_date(self):
        # downfilter the pizza df by pizzas that occured on the target date
        # start epoch is the start time converted to seconds since epoch
        self.pizza_df['start_epoch'] = self.pizza_df['queued_at'].map(
            lambda x: convert_to_epoch(x))
        print('CONVERTED COLUMN')
        self.pizza_df = self.pizza_df[(self.pizza_df['start_epoch'] >= self.day_start) & (
            self.pizza_df['start_epoch'] <= self.day_end)]

    def get_oven_pizza_ids(self):
        # make a dict with keys as oven names and vals as lists of pizza ids
        # that were made that day.
        oven_pizza_ids = {}
        ovens = ['oven-1', 'oven-2', 'oven-3', 'oven-4']

        for oven in ovens:
            start = self.date + ' 00:00:00'
            end = self.date + ' 23:59:59'
            tmp_data = DataRun(['pizza_id', 'state'], start, end, oven)
            print(tmp_data.df)
            # find if the oven was ever in entry routine during pizza id and add those ids to a list
            ids = []
            for _, row in tmp_data.df.iterrows():
                if row['state'] == 'entry_routine':
                    ids.append(int(row['pizza_id']))
                ids = list(set(ids))
            print(ids)

            # oven_pizza_ids[oven] = list(set(tmp_data.df['pizza_id']))
            oven_pizza_ids[oven] = ids
        # for ids in oven_pizza_ids:
        #     oven_pizza_ids[ids].sort()
        pp = pprint.PrettyPrinter(width=100, compact=True)
        pp.pprint(oven_pizza_ids)
        return oven_pizza_ids

    def sort_images_by_oven(self, destination_folder=None):
        # use oven pizza ids to sort the images into subfolders
        if destination_folder is None:
            destination_folder = os.path.abspath(os.path.join(self.image_folder, '..\..'))

        # create folders
        ovens = ['oven-1', 'oven-2', 'oven-3', 'oven-4']
        for oven in ovens:
            destination = os.path.abspath(os.path.join(destination_folder, oven))
            if not path.exists(destination):
                os.mkdir(destination)
        print(destination_folder)
        # loop through the pizza ids and copy a matching file into the folder
        for oven, ids in self.oven_pizza_ids.items():
            dest_dir = os.path.abspath(os.path.join(destination_folder, oven))
            print(dest_dir)
            for id in ids:
                built_str = r'\**\launcher-kid-' + \
                    str(id) + '-exit*'
                print(destination_folder + built_str)
                for file_name in glob.glob(destination_folder + built_str, recursive=True):
                    print(file_name)
                    shutil.copy(file_name, dest_dir)

    def _create_pizzas(self):

        # each row will contain id, cooking_at, completed_at, canceled_at, recipe_name, state
        for _, row in self.pizza_df.iterrows():
            oven_id = None
            for oven_name, oven_ids in self.oven_pizza_ids.items():
                if row['id'] in oven_ids:
                    oven_id = oven_name
            print(row['id'])
            self.pizzas.append(Pizza(kitchen_data=row,
                                     image_folder=self.image_folder,
                                     oven_id=oven_id))
