
# Cook Reporting

## Goal
* pull data from influxdb and the other database and create an interactive report of each cook day
* store these reports either as pdfs or interactive data reports that can be accessed in the future
 
## info
* Pulls time series data from kitchen-data.corp.serveautomation.com. Note this is not a live server. It has the data backed up to it every night
* Use cyberduck to pull images and place them in the correct folder
    - Use the [Vision repo](https://gitlab.com/serveautomation/kitchen/vision) to create a collage
    - Run a docker container and use the following command
    - python vision/collage.py --input-glob=collage/YYYY-MM-DD/launcher-gantry-*-exit*.jpg --skip-undetected --output-image-verbosity=4

## questions
* how to make this into a docker container that pulls another container and runs it (vision)

## organization
* DataRun is a tool for pulling data from influxDB
* CookDay is an object that is meant to contain all sub-components for the report of the day
    - Contains many pizza objects, data summary, and links to images

## general method
* for now will run as an executable from the command line
* input will be the date of test
* will identify the id of the first and last pizza cooked that day and use those to determine time stamps
* using those time stamps will pull overall data into a summary page including:
* * number of pizzas
* * number successful vs failed
* * creates a chart of the success vs fail
* * pizza collage
* * takt, average time a pizza is in the machine, fastest and slowest pizzas
* will then have a dropdown for each individual pizza containing
* * image
* * start and end time
* * time in each station
* * graph of oven temperatures throughout cook
* * result: succeed / cancel
* * if cancelled, what station was it in when it was cancelled?
* can have a page for each station with utilization metrics and others
* * from the time of the first pizza id compare time in entry routine + exit routine vs all else
* * have dropdown with all of the channels from that station that you can add to a graph
* * ability to add graphs

## dash app info
* has tab for each section of the app: log_updater, report, any other tools
* * log updater tab will have dropdowns or selectors for each option and a run button
* * report tab will let you select a date and choose what information you want and then run the report